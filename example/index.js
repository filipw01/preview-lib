import { configurePreview, reactConfig } from 'preview';

const dependenciesMap = {
  react: 'https://unpkg.com/react@16.9.0-alpha.0/umd/react.development',
  'react-dom': 'https://unpkg.com/react-dom@16.9.0-alpha.0/umd/react-dom.development',
  axios: 'https://unpkg.com/browse/axios@0.19.2/dist/axios',
};

configurePreview({ loaders: reactConfig.loaders, debug: true, dependenciesMap });


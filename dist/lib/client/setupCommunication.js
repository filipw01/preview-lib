"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
function setupCommunication(reload, debugLog) {
    function sendMessage(type, data) {
        debugLog("[OUT] Message from preview: " + JSON.stringify({ type: type, data: data }));
        window.parent.postMessage(__assign({ type: type }, data), '*');
    }
    /**
     * Handle general errors that are not being logged using the console
     */
    window.addEventListener('error', function () {
        sendMessage('error');
    });
    /**
     * Monkey patch console methods so that they send a message to the parent
     */
    function monkeyPatchConsole(method) {
        var original = method;
        method = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            sendMessage('log', { method: method });
            original.apply(console, args);
        };
    }
    monkeyPatchConsole(console.error);
    monkeyPatchConsole(console.warn);
    monkeyPatchConsole(console.log);
    function loadSolution(solution) {
        if (solution) {
            try {
                reload(solution);
            }
            catch (err) {
                console.error(err);
            }
            sendMessage('solutionLoaded');
        }
    }
    window.addEventListener('message', function (message) {
        debugLog("[IN] Message to preview: " + JSON.stringify(message));
        if (message.data.files) {
            loadSolution(message.data.files);
        }
        if (message.data.solution) {
            loadSolution({ 'index.js': message.data.solution });
        }
    });
    /**
     * The iframe content is loaded at this point
     */
    sendMessage('loaded');
    return sendMessage;
}
exports.default = setupCommunication;
//# sourceMappingURL=setupCommunication.js.map
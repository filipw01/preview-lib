"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var loaders_1 = require("./loaders");
var vueLoader = loaders_1.default.vueLoader, jsxVueLoader = loaders_1.default.jsxVueLoader, cssLoader = loaders_1.default.cssLoader, jsonLoader = loaders_1.default.jsonLoader;
exports.loaders = [vueLoader, jsxVueLoader, cssLoader, jsonLoader];
// This is for development only, if you want to publish preview please upload required files with it
exports.dependenciesMap = {
    vue: 'https://unpkg.com/vue@2.6.10/dist/vue',
};
//# sourceMappingURL=vueConfig.js.map
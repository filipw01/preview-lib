"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.jsSimpleProject = {
    dependenciesMap: {
        react: 'https://unpkg.com/react@15.3.2/dist/react',
        'react-dom': 'https://unpkg.com/react-dom@15.3.2/dist/react-dom',
    },
    files: {
        'index.js': "\n      import './styles/index.css';\n      import getNiceText from './utils.js';\n\n\n    ",
        'utils.js': "\n      function getNiceText(text) {\n        return text.trim().toLowerCase();\n      }\n\n      export default getNiceText;\n    ",
        'styles/index.css': "\n      h2 {\n        font-size: 40px;\n      }\n      body {\n        background-color: black;\n        color: white;\n      }\n    ",
        'styles/stuff.json': "\n      {\n        \"texts\": [\"HI\", \"TeEXT\", \"Babbbel\"]\n      }\n    ",
    },
};
exports.vueFiles = {
    'index.js': "\n    import Vue from 'vue';\n    import App from 'App.vue';\n\n    const config = {\n      ...App,\n      el: '#root',\n    };\n    \n    new Vue(config);\n  ",
    'App.vue': "\n    <script>\n    import getNiceText from './utils.js';\n\n    export default {\n      name: 'App',\n      data: {\n        text: 'Some nice text',\n      },\n      computed: {\n        niceText() {\n          return getNiceText(this.text);\n        }\n      }\n    }\n    </script>\n\n    <style>\n    .text {\n      font-size: 2rem;\n      color: tomato;\n    }\n    </style>\n\n    <template>\n      <div class=\"text\">\n      {{text}}\n      </div>\n    </template>\n  ",
    'utils.js': "\n    function getNiceText(text) {\n      return text.trim().toUpperCase();\n    }\n\n    export default getNiceText;\n  ",
    'styles/index.css': "\n    h2 {\n      font-size: 40px;\n    }\n  ",
    'styles/stuff.json': "\n    {\n      \"texts\": [\"HI\", \"TeEXT\", \"Babbbel\"]\n    }\n  ",
};
exports.reactFiles = {
    'index.jsx': "\n    import React from 'react';\n    import ReactDOM from 'react-dom';\n    import App from 'App.jsx';\n\n    ReactDOM.render(<App/>, document.getElementById('root'));\n  ",
    'App.jsx': "\n    import React from 'react';\n    import stuff from './stuff.json';\n    import { getNiceText } from './utils.js';\n\n    class App extends React.Component { \n      render() {\n        return (<ul class=\"nice-list\">\n                {stuf.texts.map(text => <li>{text}</li>)}\n              </ul>);\n      }\n    }\n\n    export default App;\n  ",
    'utils.js': "\n    export function getNiceText(text) {\n      return text.trim().toUpperCase();\n    }\n  ",
    'styles/index.css': "\n    nice-list {\n      font-size: 20px;\n      color: tomato;\n    }\n  ",
    'stuff.json': "\n    {\n      \"texts\": [\"HI\", \"TeEXT\", \"Babbbel\"]\n    }\n  ",
};
//# sourceMappingURL=filesMocks.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var babelUtils_1 = require("../utils/babelUtils");
var loaderUtils_1 = require("../utils/loaderUtils");
var TsLoader = {
    test: /.*\.ts$/,
    load: function (fileName, content) {
        var presets = ['typescript'];
        var moduleDefinition = babelUtils_1.transform(content, fileName, { presets: presets }).code;
        return loaderUtils_1.packModule(fileName, moduleDefinition);
    },
};
exports.default = TsLoader;
//# sourceMappingURL=TsLoader.js.map
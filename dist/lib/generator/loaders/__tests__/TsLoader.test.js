"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TsLoader_1 = require("../TsLoader");
var utils_1 = require("./utils");
describe('TsLoader', function () {
    var file = {
        fileName: 'index.ts',
        content: "\n      import sort from 'sorting-library';\n\n      enum FileType {\n        json = 'json',\n        css = 'css'\n      }\n\n      interface File { \n        fileName: string,\n        content: string,\n        type: FileType\n      }\n\n      function sortFiles(files: File[]): File[] {\n        return [...files].sort();\n      }\n\n      export const SOME_CONSTANT = 123;\n\n      export default sortFiles;\n    ",
    };
    it('match snapshot for ts file', function () {
        var result = TsLoader_1.default.load(file.fileName, file.content);
        expect(result).toMatchSnapshot();
    });
    utils_1.generateTestsForMatcher(TsLoader_1.default, 'ts');
});
//# sourceMappingURL=TsLoader.test.js.map
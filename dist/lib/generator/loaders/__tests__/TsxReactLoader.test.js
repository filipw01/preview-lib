"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TsxReactLoader_1 = require("../TsxReactLoader");
var utils_1 = require("./utils");
describe('TsxReactLoader', function () {
    var file = {
        fileName: 'index.tsx',
        content: "\n      import React from 'react';\n      import sort from 'sorting-library';\n      import Component from './component.ts';\n\n      enum FileType {\n        json = 'json',\n        css = 'css'\n      }\n\n      interface File {\n        fileName: string,\n        content: string,\n        type: FileType\n      }\n\n      function sortFiles(files: File[]): File[] {\n        return [...files].sort();\n      }\n\n      export class Files extends React.Component {\n        // test class properties\n        doSomeStuff = () => { this.setState({stuff: true}) };\n\n        render() {\n          // test spread;\n          const x = ['a', 'b', 'c'];\n          const y = [...x, 'd'];\n          const { files } = this.props;\n          return <div>Lol</div>;\n        }\n      }\n\n      export const SOME_CONSTANT = 123;\n\n      export default sortFiles;\n      ",
    };
    it('match snapshot for tsx file', function () {
        var result = TsxReactLoader_1.default.load(file.fileName, file.content);
        expect(result).toMatchSnapshot();
    });
    it('will fail with only ts file', function () {
        expect(function () { return TsxReactLoader_1.default.load('index.ts', file.content); }).toThrowError();
    });
    utils_1.generateTestsForMatcher(TsxReactLoader_1.default, ['tsx']);
});
//# sourceMappingURL=TsxReactLoader.test.js.map
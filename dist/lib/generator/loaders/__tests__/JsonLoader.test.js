"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var JsonLoader_1 = require("../JsonLoader");
var utils_1 = require("./utils");
describe('JsonLoader', function () {
    var file = {
        fileName: 'posts.json',
        content: "\n      {\n        \"say\": \"hi\"\n      }\n    ",
    };
    var complexFile = {
        fileName: 'complex.json',
        content: "\n    [\n      {\n        \"id\": \"0001\",\n        \"type\": \"donut\",\n        \"name\": \"Cake\",\n        \"ppu\": 0.55,\n        \"batters\":\n          {\n            \"batter\":\n              [\n                { \"id\": \"1001\", \"type\": \"Regular\" },\n                { \"id\": \"1002\", \"type\": \"Chocolate\" },\n                { \"id\": \"1003\", \"type\": \"Blueberry\" },\n                { \"id\": \"1004\", \"type\": \"Devil's Food\" }\n              ]\n          },\n        \"topping\":\n          [\n            { \"id\": \"5001\", \"type\": \"None\" },\n            { \"id\": \"5002\", \"type\": \"Glazed\" },\n            { \"id\": \"5005\", \"type\": \"Sugar\" },\n            { \"id\": \"5007\", \"type\": \"Powdered Sugar\" },\n            { \"id\": \"5006\", \"type\": \"Chocolate with Sprinkles\" },\n            { \"id\": \"5003\", \"type\": \"Chocolate\" },\n            { \"id\": \"5004\", \"type\": \"Maple\" }\n          ]\n      },\n      {\n        \"id\": \"0002\",\n        \"type\": \"donut\",\n        \"name\": \"Raised\",\n        \"ppu\": 0.55,\n        \"batters\":\n          {\n            \"batter\":\n              [\n                { \"id\": \"1001\", \"type\": \"Regular\" }\n              ]\n          },\n        \"topping\":\n          [\n            { \"id\": \"5001\", \"type\": \"None\" },\n            { \"id\": \"5002\", \"type\": \"Glazed\" },\n            { \"id\": \"5005\", \"type\": \"Sugar\" },\n            { \"id\": \"5003\", \"type\": \"Chocolate\" },\n            { \"id\": \"5004\", \"type\": \"Maple\" }\n          ]\n      },\n      {\n        \"id\": \"0003\",\n        \"type\": \"donut\",\n        \"name\": \"Old Fashioned\",\n        \"ppu\": 0.55,\n        \"batters\":\n          {\n            \"batter\":\n              [\n                { \"id\": \"1001\", \"type\": \"Regular\" },\n                { \"id\": \"1002\", \"type\": \"Chocolate\" }\n              ]\n          },\n        \"topping\":\n          [\n            { \"id\": \"5001\", \"type\": \"None\" },\n            { \"id\": \"5002\", \"type\": \"Glazed\" },\n            { \"id\": \"5003\", \"type\": \"Chocolate\" },\n            { \"id\": \"5004\", \"type\": \"Maple\" }\n          ]\n      }\n    ]\n    ",
    };
    var brokenFile = {
        fileName: 'posts.json',
        content: "\n      {\n        \"say\": \"hi\"\n      \n      ",
    };
    it('match snapshot for json file', function () {
        var result = JsonLoader_1.default.load(file.fileName, file.content);
        expect(result).toMatchSnapshot();
    });
    it('match snapshot for complex json file', function () {
        var result = JsonLoader_1.default.load(complexFile.fileName, complexFile.content);
        expect(result).toMatchSnapshot();
    });
    it('will fail with error when the JSON is broken', function () {
        expect(function () { return JsonLoader_1.default.load(brokenFile.fileName, brokenFile.content); }).toThrowError();
    });
    utils_1.generateTestsForMatcher(JsonLoader_1.default, 'json');
});
//# sourceMappingURL=JsonLoader.test.js.map
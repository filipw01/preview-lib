"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vueBabelPresetJsx = require("@vue/babel-preset-jsx");
var babelUtils_1 = require("../utils/babelUtils");
var loaderUtils_1 = require("../utils/loaderUtils");
var JsxVueLoader = {
    // it works with not only jsx but also js
    test: /.*\.jsx?$/,
    load: function (fileName, content) {
        babelUtils_1.registerPreset('vue-babel-preset-jsx', vueBabelPresetJsx);
        var presets = ['vue-babel-preset-jsx'];
        var plugins = ['proposal-class-properties', 'syntax-object-rest-spread'];
        var moduleDefinition = babelUtils_1.transform(content, fileName, {
            presets: presets,
            plugins: plugins,
        }).code;
        return loaderUtils_1.packModule(fileName, moduleDefinition);
    },
};
exports.default = JsxVueLoader;
//# sourceMappingURL=JsxVueLoader.js.map
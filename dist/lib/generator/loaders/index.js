"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueLoader_1 = require("./VueLoader");
var JsxVueLoader_1 = require("./JsxVueLoader");
var CssLoader_1 = require("./CssLoader");
var JsLoader_1 = require("./JsLoader");
var JsonLoader_1 = require("./JsonLoader");
var TsLoader_1 = require("./TsLoader");
var JsxReactLoader_1 = require("./JsxReactLoader");
var TsxReactLoader_1 = require("./TsxReactLoader");
exports.default = {
    vueLoader: VueLoader_1.default,
    jsxVueLoader: JsxVueLoader_1.default,
    cssLoader: CssLoader_1.default,
    jsonLoader: JsonLoader_1.default,
    jsLoader: JsLoader_1.default,
    tsLoader: TsLoader_1.default,
    jsxLoader: JsxReactLoader_1.default,
    tsxLoader: TsxReactLoader_1.default,
};
//# sourceMappingURL=index.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var loaderUtils_1 = require("./loaderUtils");
function createPreview(moduleDefinitions, dependenciesMap) {
    var stringifyDependenciesMap = JSON.stringify(dependenciesMap);
    var code = moduleDefinitions.join('\n');
    return "<html>\n                <head>\n                    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.js\"></script>\n                    <script>requirejs.config({\n                            paths: " + stringifyDependenciesMap + ",\n                            packages: [\"" + loaderUtils_1.PACKAGE_NAME + "\"]\n                        });\n                    </script>\n                    <script>" + code + "</script>\n                    <script>\n                    function docReady(fn) {\n                        if (document.readyState === \"complete\" || document.readyState === \"interactive\") {\n                            setTimeout(fn, 0);\n                        } else {\n                            document.addEventListener(\"DOMContentLoaded\", fn);\n                        }\n                    }\n                    </script>\n                </head>\n                <body>\n                    <div id=\"root\"></div>\n                    <script>\n                        docReady(async function() {\n                            window.requirejs(['codility-solution/index.js'], function mountingPoint() {});\n                        });\n                    </script>\n                </body>\n            </html>\n            ";
}
exports.default = createPreview;
//# sourceMappingURL=createPreview.js.map
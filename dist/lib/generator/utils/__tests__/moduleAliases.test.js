"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moduleAliases_1 = require("../moduleAliases");
describe('getModuleAliasesFromFiles', function () {
    it('generates module definitions which match snapshot', function () {
        var files = {
            'index.js': '',
            'solution.js': '',
            'solution.ts': '',
            'component.tsx': '',
        };
        var aliasModuleDefinitions = moduleAliases_1.getModuleAliasesFromFiles(files);
        expect(aliasModuleDefinitions).toMatchSnapshot();
    });
});
describe('getUniqueFileNamesWithoutExts', function () {
    it('returns unique file names without file extensions', function () {
        var fileNames = {
            'index.js': '',
            'index.ts': '',
            'hi.js': '',
            'test.json': '',
        };
        expect(moduleAliases_1.getUniqueFileNamesWithoutExts(fileNames)).toEqual(['index', 'hi', 'test']);
    });
});
describe('findAliasForName', function () {
    it('return first possible alias from files by specific order', function () {
        var fileNames = {
            'index.js': '',
            'index.ts': '',
            'hi.js': '',
            'test.json': '',
        };
        expect(moduleAliases_1.findAliasForName('index', fileNames)).toEqual('index.js');
    });
});
describe('createAliasAMDModule', function () {
    it('match snapshot for index.js alias', function () {
        var result = moduleAliases_1.createAliasAMDModule('index', 'index.js');
        expect(result).toMatchSnapshot();
    });
});
//# sourceMappingURL=moduleAliases.test.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PACKAGE_NAME = 'codility-solution';
exports.INTERNAL_PACKAGE = '@@@internals';
function fileNameWithPackageRoot(fileName) {
    return exports.PACKAGE_NAME + "/" + fileName;
}
exports.fileNameWithPackageRoot = fileNameWithPackageRoot;
function fileNameAsInternalPackage(fileName) {
    return exports.INTERNAL_PACKAGE + "/" + fileName;
}
exports.fileNameAsInternalPackage = fileNameAsInternalPackage;
function nameAMDModule(packageName, module) {
    var startString = 'define([';
    if (module.indexOf(startString) === 1) {
        throw new Error('Transpiled module is not AMD. Something is wrong with babel plugins.');
    }
    return module.replace('define([', "define(\"" + packageName + "\",[");
}
exports.nameAMDModule = nameAMDModule;
function packModule(packageName, module) {
    return nameAMDModule(fileNameWithPackageRoot(packageName), module);
}
exports.packModule = packModule;
//# sourceMappingURL=loaderUtils.js.map
export declare function registerPlugin(name: string, plugin: object): void;
export declare function registerPreset(name: string, preset: object): void;
export declare function transform(code: string, fileName: string, options?: {}): {
    code: string;
};

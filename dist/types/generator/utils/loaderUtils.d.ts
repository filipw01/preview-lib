export declare const PACKAGE_NAME = "codility-solution";
export declare const INTERNAL_PACKAGE = "@@@internals";
export declare function fileNameWithPackageRoot(fileName: string): string;
export declare function fileNameAsInternalPackage(fileName: string): string;
export declare function nameAMDModule(packageName: string, module: string): string;
export declare function packModule(packageName: string, module: string): string;

import { Files } from '../../@types/files';
export declare const DEFAULT_EXTENSIONS_ORDER: string[];
export declare function getUniqueFileNamesWithoutExts(files: Files): Array<string>;
export declare function findAliasForName(name: string, files: Files): string | undefined;
export declare function createAliasAMDModule(alias: string, moduleWithAlias: string): string;
export declare function getModuleAliasesFromFiles(files: Files): string[];

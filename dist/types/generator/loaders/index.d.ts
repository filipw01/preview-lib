declare const _default: {
    vueLoader: import("../../@types/loader").Loader;
    jsxVueLoader: import("../../@types/loader").Loader;
    cssLoader: import("../../@types/loader").Loader;
    jsonLoader: import("../../@types/loader").Loader;
    jsLoader: import("../../@types/loader").Loader;
    tsLoader: import("../../@types/loader").Loader;
    jsxLoader: import("../../@types/loader").Loader;
    tsxLoader: import("../../@types/loader").Loader;
};
export default _default;

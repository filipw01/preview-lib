import PreviewGenerator from '../PreviewGenerator';
import loaders from '../loaders';
import { vueConfig, reactConfig } from '..';
import { jsSimpleProject, vueFiles, reactFiles } from './filesMocks';

describe('PreviewGenerator', () => {
  it('match snapshot for common js project', () => {
    const { dependenciesMap, files } = jsSimpleProject;
    const { jsLoader, cssLoader, jsonLoader } = loaders;
    const generator = new PreviewGenerator({
      loaders: [jsLoader, cssLoader, jsonLoader],
      dependenciesMap,
    });
    const result = generator.generate(files);
    expect(result).toMatchSnapshot();
  });

  it('match snapshot for vue project', () => {
    const vuePreviewGenerator = new PreviewGenerator(vueConfig);
    const result = vuePreviewGenerator.generate(vueFiles);
    expect(result).toMatchSnapshot();
  });

  it('match snapshot for react project', () => {
    const reactPreviewGenerator = new PreviewGenerator(reactConfig);
    const result = reactPreviewGenerator.generate(reactFiles);
    expect(result).toMatchSnapshot();
  });

  it('should run load methods of loaders for specific files', () => {
    const { jsLoader, cssLoader, jsonLoader } = loaders;
    const mockJSLoader = { ...jsLoader, load: jest.fn(() => '') };
    const mockCSSLoader = { ...cssLoader, load: jest.fn(() => '') };
    const mockJSONLoader = { ...jsonLoader, load: jest.fn(() => '') };
    const mockedLoaders = [mockJSLoader, mockCSSLoader, mockJSONLoader];
    const generator = new PreviewGenerator({ loaders: mockedLoaders, dependenciesMap: {} });

    const files = {
      'index.css': '',
      'index.js': '',
      'index.json': '',
    };

    generator.generate(files);

    expect(mockJSONLoader.load).toHaveBeenCalledTimes(1);
    expect(mockCSSLoader.load).toHaveBeenCalledTimes(1);
    expect(mockJSLoader.load).toHaveBeenCalledTimes(1);
  });

  it('should run only first loader for specific file extension', () => {
    const { jsLoader, jsxLoader } = loaders;
    const mockJSLoader = { ...jsLoader, load: jest.fn(() => '') };
    const mockJSXLoader = { ...jsxLoader, load: jest.fn(() => '') };
    const mockedLoaders = [mockJSLoader, mockJSXLoader];
    const generator = new PreviewGenerator({ loaders: mockedLoaders, dependenciesMap: {} });

    const files = {
      'index.js': '',
    };

    generator.generate(files);

    expect(mockJSLoader.load).toHaveBeenCalledTimes(1);
    expect(mockJSXLoader.load).toHaveBeenCalledTimes(0);
  });
});

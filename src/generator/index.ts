export { default as PreviewGenerator } from './PreviewGenerator';
export { default as loaders } from './loaders';
export { packModule } from './utils/loaderUtils';
export { registerPlugin, registerPreset, transform } from './utils/babelUtils';
import { dependenciesMap as reactDependenciesMap, loaders as reactLoaders } from './reactConfig';
import { dependenciesMap as vueDependenciesMap, loaders as vueLoaders } from './vueConfig';

export const reactConfig = {
  loaders: reactLoaders,
  dependenciesMap: reactDependenciesMap,
};

export const vueConfig = {
  loaders: vueLoaders,
  dependenciesMap: vueDependenciesMap,
};

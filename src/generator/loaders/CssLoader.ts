import injectStyles from '../utils/injectStyles';
import { fileNameWithPackageRoot } from '../utils/loaderUtils';
import { Loader } from '../../@types/loader';

const CssLoader: Loader = {
  test: /.*\.css$/,
  load(fileName: string, content: string): string {
    // create AMD module that will inject styles to document on load
    return `define("${fileNameWithPackageRoot(fileName)}", [], function () {
            ${injectStyles(content)}
        });`;
  },
};

export default CssLoader;

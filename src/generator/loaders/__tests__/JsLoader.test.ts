import jsLoader from '../JsLoader';
import { generateTestsForMatcher } from './utils';

describe('JsLoader', () => {
  const file = {
    fileName: 'index.js',
    content: `
      import sort from 'sorting-library';

      function sortFiles(files) {
        return [...files].sort();
      }

      export const SOME_CONSTANT = 123;

      export default sortFiles;
    `,
  };

  it('match snapshot for js file', () => {
    const result = jsLoader.load(file.fileName, file.content);
    expect(result).toMatchSnapshot();
  });

  generateTestsForMatcher(jsLoader, 'js');
});

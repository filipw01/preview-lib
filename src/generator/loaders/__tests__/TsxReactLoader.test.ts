import tsxLoader from '../TsxReactLoader';
import { generateTestsForMatcher } from './utils';

describe('TsxReactLoader', () => {
  const file = {
    fileName: 'index.tsx',
    content: `
      import React from 'react';
      import sort from 'sorting-library';
      import Component from './component.ts';

      enum FileType {
        json = 'json',
        css = 'css'
      }

      interface File {
        fileName: string,
        content: string,
        type: FileType
      }

      function sortFiles(files: File[]): File[] {
        return [...files].sort();
      }

      export class Files extends React.Component {
        // test class properties
        doSomeStuff = () => { this.setState({stuff: true}) };

        render() {
          // test spread;
          const x = ['a', 'b', 'c'];
          const y = [...x, 'd'];
          const { files } = this.props;
          return <div>Lol</div>;
        }
      }

      export const SOME_CONSTANT = 123;

      export default sortFiles;
      `,
  };

  it('match snapshot for tsx file', () => {
    const result = tsxLoader.load(file.fileName, file.content);
    expect(result).toMatchSnapshot();
  });

  it('will fail with only ts file', () => {
    expect(() => tsxLoader.load('index.ts', file.content)).toThrowError();
  });

  generateTestsForMatcher(tsxLoader, ['tsx']);
});

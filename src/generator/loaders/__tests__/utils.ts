import { Loader } from '../../../@types/loader';

const fileExtensions = ['js', 'jsx', 'ts', 'tsx', 'json', 'svg', 'css'];

export function generateTestsForMatcher(loader: Loader, ext: string | string[]): void {
  const goodExtensions = !Array.isArray(ext) ? [ext] : ext;
  const notGoodExtensions = fileExtensions.filter(
    currExt => goodExtensions.indexOf(currExt) === -1,
  );

  it(`test regexp should match only ${goodExtensions.join(' / ')} files`, () => {
    const regexp = loader.test;

    // good cases
    goodExtensions.forEach(currGoodExt => {
      expect(`lol.${currGoodExt}`.match(regexp)).toBeTruthy();
      expect(`path/lol.${currGoodExt}`.match(regexp)).toBeTruthy();
      expect(`codility-solution/path/lol.${currGoodExt}`.match(regexp)).toBeTruthy();
      expect(`lol.${currGoodExt.slice(currGoodExt.length - 1)}`.match(regexp)).not.toBeTruthy();
    });

    // bad real cases
    goodExtensions.forEach(currGoodExt => {
      notGoodExtensions.forEach(currBadExt => {
        expect(`lol.${currGoodExt}.${currBadExt}`.match(regexp)).not.toBeTruthy();
        expect(`path/lol.${currGoodExt}.${currBadExt}`.match(regexp)).not.toBeTruthy();
        expect(
          `codility-solution/path/lol.${currGoodExt}.${currBadExt}`.match(regexp),
        ).not.toBeTruthy();
      });
    });

    notGoodExtensions.forEach(currExt => {
      expect(`file.${currExt}`.match(regexp)).not.toBeTruthy();
    });

    // weird cases
    expect('.'.match(regexp)).not.toBeTruthy();
    expect(''.match(regexp)).not.toBeTruthy();
    expect('hi'.match(regexp)).not.toBeTruthy();
    expect('$'.match(regexp)).not.toBeTruthy();
  });
}

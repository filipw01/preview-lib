import { transform } from '../utils/babelUtils';
import { packModule } from '../utils/loaderUtils';
import { Loader } from '../../@types/loader';

// it works with not only jsx but also js
const JsxReactLoader: Loader = {
  test: /.*\.jsx?$/,
  load(fileName: string, content: string) {
    const presets = ['react'];
    const plugins = ['proposal-class-properties', 'syntax-object-rest-spread'];
    const { code: moduleDefinition } = transform(content, fileName, {
      presets,
      plugins,
    });

    return packModule(fileName, moduleDefinition);
  },
};
export default JsxReactLoader;

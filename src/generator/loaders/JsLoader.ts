import { transform } from '../utils/babelUtils';
import { packModule } from '../utils/loaderUtils';
import { Loader } from '../../@types/loader';

const JsLoader: Loader = {
  test: /.*\.js$/,
  load(fileName: string, content: string): string {
    const { code: moduleDefinition } = transform(content, fileName);
    return packModule(fileName, moduleDefinition);
  },
};

export default JsLoader;

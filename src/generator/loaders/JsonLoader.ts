import { transform } from '../utils/babelUtils';
import { packModule } from '../utils/loaderUtils';
import { Loader } from '../../@types/loader';

const JsonLoader: Loader = {
  test: /.*\.json$/,
  load(fileName: string, content: string): string {
    try {
      // Parse to validate JSON
      JSON.parse(content);
      const wrappedJSON = `export default ${content};`;

      const { code: moduleDefinition } = transform(wrappedJSON, fileName);

      return packModule(fileName, moduleDefinition);
    } catch (error) {
      throw new Error(`${fileName} has error in the structure. Details: ${error.message}`);
    }
  },
};

export default JsonLoader;

import { PACKAGE_NAME } from './loaderUtils';
import { DependenciesMap } from '../../@types/dependenciesMap';

export default function createPreview(
  moduleDefinitions: string[],
  dependenciesMap: DependenciesMap,
): string {
  const stringifyDependenciesMap = JSON.stringify(dependenciesMap);
  const code = moduleDefinitions.join('\n');

  return `<html>
                <head>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.js"></script>
                    <script>requirejs.config({
                            paths: ${stringifyDependenciesMap},
                            packages: ["${PACKAGE_NAME}"]
                        });
                    </script>
                    <script>${code}</script>
                    <script>
                    function docReady(fn) {
                        if (document.readyState === "complete" || document.readyState === "interactive") {
                            setTimeout(fn, 0);
                        } else {
                            document.addEventListener("DOMContentLoaded", fn);
                        }
                    }
                    </script>
                </head>
                <body>
                    <div id="root"></div>
                    <script>
                        docReady(async function() {
                            window.requirejs(['codility-solution/index.js'], function mountingPoint() {});
                        });
                    </script>
                </body>
            </html>
            `;
}

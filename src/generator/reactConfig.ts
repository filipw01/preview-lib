import fileLoaders from './loaders';

const { jsxLoader, cssLoader, jsonLoader } = fileLoaders;

export const loaders = [jsxLoader, cssLoader, jsonLoader];

// This is for development only, if you want to publish preview please upload required files with it
export const dependenciesMap = {
  react: 'https://unpkg.com/react@16.10.1/umd/react.development',
  'react-dom': 'https://unpkg.com/react-dom@16.10.1/umd/react-dom.development',
};

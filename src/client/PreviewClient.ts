import setupCommunication from './setupCommunication';
import PreviewGenerator from '../generator/PreviewGenerator';
import { Files } from '../@types/files';
import { ClientOptions } from '../@types/clientOptions';

export default class PreviewClient {
  constructor(options: ClientOptions) {
    const { previewGenerator, debug } = options;
    this.sendMessage = null;
    this.debug = debug || false;
    this.previewGenerator = previewGenerator;
  }

  previewGenerator: PreviewGenerator;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  sendMessage: ((type: string, data?: any) => void) | null;

  debug: boolean;

  updateIframe(html: string): void {
    this.debugLog('Updating preview iframe.');
    const iframe = document.getElementById('preview');

    if (iframe) {
      iframe.setAttribute('srcdoc', html);
    } else {
      const newIframe = document.createElement('iframe');
      newIframe.setAttribute('id', 'preview');
      newIframe.setAttribute('srcdoc', html);
      const body = document.body || document.getElementsByTagName('body')[0];
      body.setAttribute('style', 'margin: 0;');
      body.appendChild(newIframe);
    }

    this.debugLog('Iframe has been updated.');
  }

  reload = (solutionSource: Files): void => {
    this.debugLog('Transpiling code.');
    try {
      this.updateIframe(this.previewGenerator.generate(solutionSource));
    } catch (error) {
      console.error(error);
    }
    this.debugLog('Code has been transpiled.');
  };

  init(): void {
    const styleRules = document.createElement('style');
    styleRules.innerHTML = `#preview { width: 100vw; height: 100vh; border: 0; }
      body { margin: 0; }
    `;
    const head = document.head || document.getElementsByTagName('head')[0];
    head.appendChild(styleRules);
    this.sendMessage = setupCommunication(this.reload, this.debugLog);
  }

  debugLog = (message: string): void => {
    if (this.debug) {
      console.log(message);
    }
  };
}

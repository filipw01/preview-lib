# Preview 

This repository contains library which let's you setup communication between Codility editor and preview. 
What's more it will transpile files and load them into browser using loaders specified in [loaders directory](./src/generator/loaders). 

### Install requirements 

You need to install bunch of babel plugins, and @babel/standalone to make it run. Check package.json peer dependencies. 

### How to use it? Example code

```js
import { reactConfig, configurePreview } from 'preview';

const dependenciesMap = {
  react: 'https://unpkg.com/react@16.9.0-alpha.0/umd/react.development',
  'react-dom': 'https://unpkg.com/react-dom@16.9.0-alpha.0/umd/react-dom.development',
  axios: 'https://unpkg.com/browse/axios@0.19.2/dist/axios'
};

configurePreview({loaders: reactConfig.loaders, debug: true, dependenciesMap});
```

### NPM scripts

 - `yarn test`: Run test suite
 - `yarn start`: Run `yarn build` in watch mode
 - `yarn test:watch`: Run test suite in [interactive watch mode](http://facebook.github.io/jest/docs/cli.html#watch)
 - `yarn test:prod`: Run linting and generate coverage
 - `yarn build`: Generate bundles and typings, create docs
 - `yarn lint`: Lints code


